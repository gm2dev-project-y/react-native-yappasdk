# Developer notes
This repository is the source for the NPM module named **react-native-yappasdk**, it includes the module itself for both iOS and Android. Also contains an example app for testing.

- Node Version v14.16.1
- Package manager: Yarn


## To run the project, follow these steps:

- On the root path of this repository, run ```yarn install```, this will make sure all the Yappa SDK dependencies are installed.
- Go to the ```/example/``` folder and also run ```yarn install```, this is an example App that uses the Yappa SDK.
- Next, just run npx ```react-native run-android```.

### This project uses a native dependency (Yappa SDK), to modify it, you must compile each project individually, see:
- **Android:** https://bitbucket.org/gm2dev-project-y/yappa-sdk-android/
- **iOS:** https://bitbucket.org/gm2dev-project-y/yappa-sdk-ios/src/develop/