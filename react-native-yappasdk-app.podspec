require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-yappasdk-app"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.homepage     = package["homepage"]
  s.license      = package["license"]
  s.authors      = package["author"]

  s.platforms    = { :ios => "12.0" }
  s.source       = { :git => "https://bitbucket.org/gm2dev-project-y/react-native-yappasdk.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,mm,swift}"

  s.dependency "React-Core"
  s.dependency "YappaAppSDK"

  ############################################
  # s.vendored_frameworks = 'ios/Framework/YappaSDK.xcframework'

  #  s.dependency 'Material'
  #  s.dependency 'Alamofire', '= 5.0.0-rc.2'
  #  s.dependency 'ObjectMapper'
  #  s.dependency 'AlamofireObjectMapper'

  #  s.dependency "FlagPhoneNumber"
  #  s.dependency 'AWSLogs'
  #  s.dependency 'Firebase/Messaging'
  #  s.dependency 'Firebase/Analytics'
  #  s.dependency 'GoogleUtilities'

end
