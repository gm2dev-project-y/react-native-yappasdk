import YappaSDK
@objc(Yappasdk)
class Yappasdk: RCTEventEmitter {
    
    
    @objc override static func requiresMainQueueSetup() -> Bool {
        return false
    }
    
    @objc(handleNotification:withCallback:withReject:)
    func handleNotification(userInfo: [AnyHashable : Any], callback: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) -> Void {
        DispatchQueue.main.async {
            Yappa.handleNotification(UIApplication.shared, userInfo) { (data) in
                callback(data)
            }
        }
    }

    @objc(setFCMToken:)
    func setFCMToken(token: String) {
        Yappa.setFCMToken(token)
    }

    @objc(initialize:withAppId:)
    func initialize(hash: String, appId: String) -> Void {
        DispatchQueue.main.async {
            Yappa.initialize(hash: hash, appId: appId)
        }
    }

    @objc(setContentUrl:)
    func setContentUrl(url: String) {
        Yappa.setSiteUrl(url)
    }

    @objc(setContentId:)
    func setContentId(contentId: String) {
        Yappa.setContentId(contentId)
    }

    @objc(show)
    func show() {
        DispatchQueue.main.async {
            Yappa.openSDK();
        }
    }
    
    @objc(close)
    func close() {
        print("not available in iOS")
    }
    
    override func supportedEvents() -> [String]! {
      return ["YappaNotifHandler"]
    }

  @objc(setupObserver)
  func setupObserver() {
              NotificationCenter.default.addObserver(forName: Notification.Name("YappaNotifCenter"), 
        object: nil,
        queue: nil) {notification in
           self.sendEvent(withName: "YappaNotifHandler", body: notification.userInfo!)
        }     
  }
}

