# react-native-yappasdk-app

Yappa SDK - Audio & Video Commenting tool
v1.0.12

# Installation

```sh
npm install react-native-yappasdk-app
```

You must also install the following dependencies

```sh
npm install @react-native-firebase/app
npm install @react-native-firebase/messaging
npm install @react-native-community/push-notification-ios
```

## Android

Go to your `build.gradle` file located in `android/app/build.gradle` and add the following:

On top of your file add:

```
apply plugin: 'com.google.gms.google-services'
```

The SDK uses databinding on their views, so it must be enabled at app level as shown:

Note that if you're using Gradle 4.x.x or above you must include databinding in the following way:

```
android {

    ...

    buildFeatures {
        dataBinding true
    }

    ...
}
```

In case you're using Gradle 3.x.x you must add the following instead:

```
android {

    ...
    android.dataBinding.enabled = true
    ...
}
```

In your dependencies, you must add the following:

```
implementation 'org.bitbucket.gm2dev-project-y:yappa-sdk-android:v1.0.38'
```

Then go to your project's `build.gradle` located in `android/build.gradle` and add the following:

```
dependencies {

    ...

    classpath 'com.google.gms:google-services:4.3.4'

    ...
}
```

Lastly, you must copy your `google-services.json` (Provided by Yappa) inside of your `android/app` directory.

**Note:** We are using Firebase with BoM for Push Notifications.

### Adding Custom URL Scheme in the Project

In order to be able to login with Social Networks (Google, Facebook) inside the SDK, you must register a custom URL scheme inside your application with the following steps:

In your app's `AndroidManifest.xml` add the following inside the `<application>` tag:

```html
<activity
  android:name="com.yappa.sdk.ui.MainNavigationActivity"
  android:launchMode="singleTop"
>
  <intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data android:host="authorize" android:scheme="**YOUR_PACKAGE_ID**" />
  </intent-filter>
</activity>
```

You must change `**YOUR_PACKAGE_ID**` with your app's package identifier (e.g. com.example.app)

### **Running out of memory when building your app**

Depending on your application size, your JVM might run out of memory and throw the following error when building:

```
Expiring Daemon because JVM heap space is exhausted
java.lang.OutOfMemoryError
```

To fix this you must add the following lines in your `android/gradle.properties`:

```
org.gradle.jvmargs=-Xmx4096m -XX:MaxPermSize=4096m -XX:+HeapDumpOnOutOfMemoryError
org.gradle.daemon=true
org.gradle.parallel=true
org.gradle.configureondemand=true
```

### **Enable Multidex**

There's a chance you might run into a `DexArchiveMergerException` error, this is caused because the when your app and the native libraries it references exceed 65,536 methods.

To avoid any issues, it's recommended for you to enable multidex.

More information at: https://developer.android.com/studio/build/multidex

## iOS

First you need to install the SDK Pods

```
cd ios & pod install
```

### Notes

- Minimum target version to run the SDK is 12.0.

- If you're unable to compile the app you need to add `use_frameworks!` to the podfile located in the `ios/{PROJECT_NAME}`:

- If you encounter any issues related to UTF-8 and Podfile, run pod install using UTF-8: `LANG=en_US.UTF-8 pod install`

- If you get any errors like `Undefined symbol: __swift_FORCE_LOAD_$_swiftWebKit`, you can follow next steps to try to fix it:

  - First create an empty File.swift into your iOS Project
  - Once created you will be asked to configure an Objective-C bridging header, click "Create bridging header"

  Reference from:
  https://github.com/react-native-community/upgrade-support/issues/129

- If you get any errors like `Undefined symbol: _OBJC_CLASS _ $ _ FIRMessaging`, you can follow next steps to try to fix it:

```
    'Clear Xcode Cache'
    rm -rf ~/Library/Developer/Xcode/DerivedData

    'Delete the entire local build folder with'
    rm -rf ./ios/build

    'Clear CocoaPods Cache (into ios folder)'
    pod deintegrate && pod cache clean --all

    'Re-Install Pods'
    pod install

    'Manual links'
    react-native link @react-native-firebase/app
    react-native link @react-native-firebase/messaging

    'Add pre_install script lines in Podfile'
    pre_install do |installer|
     installer.pod_targets.each do |pod|
       if pod.name.start_with?('RNFB')
         def pod.build_type;
           Pod::BuildType.static_library
         end
       end
     end
    end

```

- If you get any errors like `dyld: Symbol not found: _$ "s15FlagPhoneNumber14FPNCountryCodeO2USyA2CmFWC (FlagPhoneNumber)"`, you can follow next steps to try to fix it:

```
    'Add post_install script lines in Podfile'
    post_install do |installer|
      installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
          config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
         end
      end
    end
```

Example of a Podfile:

```
require_relative '../node_modules/react-native/scripts/react_native_pods'
require_relative '../node_modules/@react-native-community/cli-platform-ios/native_modules'

platform :ios, '12.0'

target 'yappasdk' do
  use_frameworks!
  config = use_native_modules!

  use_react_native!(:path => config["reactNativePath"])

end
```

### Adding push notifications

First you need to enable `Push Notification` and `Background Modes` -> `Remote notifications` capabilities to allow push notifications. You can check this step in: https://developer.apple.com/documentation/xcode/adding-capabilities-to-your-app

To receive push notifications we use Firebase Cloud Messaging. This dependency is already included when installing the SDK.

### Adding native code

Go to `AppDelegate.m` located in the `ios/{PROJECT_NAME}` directory and add the following lines:

On your imports add:

```objc
#import <Firebase.h>
#import <UserNotifications/UserNotifications.h>
#import <RNCPushNotificationIOS.h>
```

Create the following interface:

```objc
@interface AppDelegate() <UNUserNotificationCenterDelegate>

@end
```

Or add `UNUserNotificationCenterDelegate` in your existing interface:

```objc
@interface AppDelegate () <RCTBridgeDelegate, UNUserNotificationCenterDelegate>


@property (nonatomic, strong) UMModuleRegistryAdapter *moduleRegistryAdapter;
@property (nonatomic, strong) NSDictionary *launchOptions;

@end
```

In your `didFinishLaunchingWithOptions` method add next lines at the beginning:

```objc
  [FIRApp configure];
  UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
  center.delegate = self;
```

Next add the following methods inside `@implementation AppDelegate` under the `didFinishLaunchingWithOptions` method that are responsible for handling notifications:

```objc
// Required for the register event.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
 [RNCPushNotificationIOS didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}
// Required for the notification event. You must call the completion handler after handling the remote notification.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [RNCPushNotificationIOS didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}
// Required for the registrationError event.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
 [RNCPushNotificationIOS didFailToRegisterForRemoteNotificationsWithError:error];
}
// Required for localNotification event
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
{
  [RNCPushNotificationIOS didReceiveNotificationResponse:response];
}
//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
  completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
}
```

Lastly, you must copy your `GoogleService-Info.plist` (Provided by Yappa) inside of your `ios` root directory.

### Adding permissions and messages

To post and reply Yaps it is mandatory to add permissions for the camera, microphone and photo library in your `Info.plist` located in `ios/{PROJECT_NAME}`

```
<key>NSCameraUsageDescription</key>
<string>Yappa would like to access your camera to record your Yaps</string>
<key>NSMicrophoneUsageDescription</key>
<string>Yappa would like to access your microphone to record your Yaps</string>
<key>NSPhotoLibraryUsageDescription</key>
<string>Yappa would like to use your picture and video library to take your Yaps</string>
```

### Adding Custom URL Scheme in the Project

In order to be able to login with Social Networks (Google, Facebook) inside the SDK, you must register a custom URL scheme inside your application with the following steps:

```
1: Open Project
2: Select Target
3: Now select Info and go to URL Types and click on '+'
4: Now in the 'URL Schemes' field enter next Custom URL scheme: 'com.example.app' and Identifier 'yappa.scheme'
```

This URL Scheme must be the same as your app's **bundleId** (e.g. com.example.app)

# Usage

In your `App.js` (or the entry point file of your React Native application) add the following:

```js
import YappaSDK from 'react-native-yappasdk-app';
```

Then in your `useEffect()` or `componentDidMount()` add the following

```js
YappaSDK.initialize('API_KEY', 'APP_ID');

YappaSDK.handleRemoteNotification(
  (contentId: string, contentUrl: string, showSdk) => {
    console.log('Received ', contentId, contentUrl);

    // Handle app navigation
    // Use your app navigation to navigate to a specific content
    // e.g. navigateToArticle(contentId, contentUrl)

    // Once the navigation is completed, show the SDK screen
    showSdk();
  }
);
```

Next, in your content screens you must add the Yappa Floating Button, this will allow the user to open the SDK when pressing on it and show all the related Yaps.

```js
import { YappaActionButton } from 'react-native-yappasdk-app';
```

Add the component `YappaActionButton` inside your content screen, this will show a Floating Button in the bottom right corner.

```js
<YappaActionButton contentUrl="YOUR_CONTENT_URL" contentId="YOUR_CONTENT_ID" />
```

- `YOUR_CONTENT_URL`: The content URL that identifies this specific content (if applies)
- `YOUR_CONTENT_ID`: Your content identifier for this specific content (if applies)

## Methods

### **YappaSDK.initialize('API_KEY', 'APP_ID')**

Initializes all the SDK module dependencies and configuration, must be called at the start of your application to make sure the SDK is available throughout the whole app.

- `API_KEY`: Your activation hash string provided by Yappa
- `APP_ID`: Your unique application identifier provided by Yappa

**Note:** If your `API_KEY` is incorrect, the SDK will fail to open.

### **YappaSDK.handleRemoteNotification(callback)**

Handles all incoming Yappa notifications, it takes a callback function as a parameter that is called when the user opens any Yappa notification.
The callback returns the following information:

- `contentId`: Unique identifier of the related content in the notification (if applies)
- `contentUrl`: Unique URL that identifies the content in the notification(if applies)
- `showSdk()`: Callback function that automatically opens the SDK screen when called, ideally it should be called after the related content is shown on your screen. Your app must have a navigation method to navigate to any content in your app.

### **YappaSDK.setContentId('CONTENT_ID')**

Sets the content identifier for the next time the SDK opens.

- `contentId`: Unique identifier of the related content (if applies)

### **YappaSDK.setContentUrl('URL')**

Sets the content identifier for the next time the SDK opens.

- `contentUrl`: Unique URL that identifies the content in (if applies)

### **YappaSDK.show()**

Forces the SDK screen to show, if the SDK is not initialized properly this method will fail with an exception.

### **YappaSDK.close()**

Forces the SDK to close.

# License

MIT
