import { NativeModules, Platform , NativeEventEmitter} from 'react-native';
import YappaActionButton from './components/common/YappaActionButton';
import messaging, { FirebaseMessagingTypes } from '@react-native-firebase/messaging';
import PushNotificationIOS, { NotificationRequest } from '@react-native-community/push-notification-ios';

type YappasdkType = {
  initialize(apiKey: string, appId: string): void;
  setContentUrl(url: string): void;
  setContentId(id: string): void;
  setFCMToken(token: string): void;
  show(): void;
  close(): void;
  handleRemoteNotification(
    callback: (
      contentId: string,
      contentUrl: string,
      showSdk: () => void
    ) => void
  ): void;
};

const { Yappasdk } = NativeModules;

const events = new NativeEventEmitter(NativeModules.Yappasdk)

const handleCachedNotification = (callback: (contentId: string, contentUrl: string, showSdk: () => void) => void) => {
  // Case when a notification was generated in the foreground and then try to open it with the app closed.
  if(Platform.OS == "android"){

    Yappasdk.getNotificationData().then((data : any) => {

      const { contentUrl, contentId } = data;
      console.log("contentUrl");
      console.log(contentUrl);

      callback(contentId || '', contentUrl || '', () => {
        console.log("callback");
        Yappasdk.initializeFromNotification();
      });
      
    })
  }
}

const handleNotification = (remoteMessage: FirebaseMessagingTypes.RemoteMessage, callback: (contentId: string, contentUrl: string, showSdk: () => void) => void) => {
  if (remoteMessage.data) {
    const contentId = remoteMessage.data.contentId;
    const contentUrl = remoteMessage.data.originalUrl;

    callback(contentId || '', contentUrl || '', () => {
      if (Platform.OS == 'ios') {
        Yappasdk.handleNotification(remoteMessage.data);
      } else {
        Yappasdk.handleNotification(remoteMessage);
      }
    });
  }
}

Yappasdk.handleRemoteNotification = (
  callback: (contentId: string, contentUrl: string, showSdk: () => void) => void
) => {

  handleCachedNotification(callback);

  if (Platform.OS == 'ios') {
    PushNotificationIOS.addEventListener(
      'localNotification',
      (notification) => {
        const data = notification.getData();
        const { originalUrl, contentId } = data;
        callback(contentId || '', originalUrl || '', () => {
          Yappasdk.handleNotification(data);
        });
      }
    );

    Yappasdk.setupObserver();

    events.addListener(
      'YappaNotifHandler',
      (value) => {
        console.log("YappaNotifHandler" )
        console.log(value )
        const { originalUrl, contentId } = value;
        callback(contentId || '', originalUrl || '', () => {
          Yappasdk.handleNotification(value);
        });
      }
  );
  } else {
    events.addListener(
      'YappaAndroidNotif',
      (value) => {
        console.log("YappaAndroidNotif" )
        console.log(value)
        const { originalUrl, contentId } = value;
        callback(contentId || '', originalUrl || '', () => {
          Yappasdk.handleNotification(value);
        });
      }
  );
  }

  messaging().requestPermission()
    .then((authStatus) => {
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;
      console.log('-- enabled', enabled);
      if (enabled) {
        messaging()
          .getToken()
          .then((fcmToken) => {
            if (fcmToken) {
              console.log('Your Firebase Token is:', fcmToken);
              Yappasdk.setFCMToken(fcmToken);
            } else {
              console.log('Failed', 'No token received');
            }
          });
      }
    });

  messaging().onMessage(async (remoteMessage) => {
    if (Platform.OS == 'ios') {
      if(remoteMessage != null && remoteMessage.notification != null){
        const detail : NotificationRequest = {
          id: remoteMessage.messageId || "",
          title: remoteMessage.notification.title,
          subtitle: '',
          body: remoteMessage.notification.body,
          userInfo: remoteMessage.data,
        };
        PushNotificationIOS.addNotificationRequest(detail);
      }

    } else {
      Yappasdk.handleNotificationForeground(remoteMessage);
      handleCachedNotification(callback);
    }
  });

  messaging().getInitialNotification().then((remoteMessage) => {
    if(Platform.OS == "android" && remoteMessage){
      handleNotification(remoteMessage, callback);
    }
  })

  messaging().onNotificationOpenedApp((remoteMessage) => {
    handleNotification(remoteMessage, callback);
  });
};

export { YappaActionButton };

export default Yappasdk as YappasdkType;
