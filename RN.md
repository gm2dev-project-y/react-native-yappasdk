## react-native-yappasdk RELEASE NOTE ##


# v1.0.12 - react-native-yappasdk  11-08-2021 ###

## iOS changes

### 1.0.0(31) - YappaAppSDK 1.0.15  11-5-2021 ###

[SDKAPP-122] - Added Flurry custom events
### 1.0.0(30) - YappaAppSDK 1.0.14  11-4-2021 ###

[SDKAPP-5]  - Push notifications are not displaying for any action
[SDKAPP-7]  - Welcome message icon has not the correct format
[SDKAPP-8]  - Added 9 to arg phone number hint text
[SDKAPP-25]  - Registration Optional inputs - Validation

## Android changes
### 1.0.48 - 11-08-2021 ###
[SDKAPP-123] - [Feature] Added Flurry custom events
### 1.0.46 - 10-01-2021 ###
[SDKAPP-100] - [Fix] Audio replays by itself after locking/unlocking device
[SDKAPP-114] - [Fix] Reply notifications lead to a deleted yap
[SDKAPP-115] - [Fix] Push notifications are crashing the app
[SDKAPP-116] - [Fix] Thread selector stops working after opening a notification

------------------------------------------------------------------------------------------------------------------------
# v1.0.11 - react-native-yappasdk  10-20-2021 ###

## iOS changes
### 1.0.0(29) - YappaAppSDK 1.0.13  10-19-2021 ###
[SDKAPP-97]  - Tapp on yapp from notification center not navigating to yapp
[SDKAPP-112] - Flurry integration
[Fix]        - Flurry module fix
[First RN]   - First release note
[pod GoogleUtilities]   - Needed for Flurry

## Android changes
### 1.0.45 - 10-18-2021 ###
[SDKAPP-96]  - Tapp on yapp from notification center not navigating to yapp
[SDKAPP-109] - Search for deleted yapp from notification on notification center
[SDKAPP-111] - Flurry integration
[First RN]   - First release note

