package com.reactnativeyappasdk;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.yappa.sdk.YappaSDK;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableMap;
import java.util.Map;
import java.util.HashMap;
import com.facebook.react.bridge.LifecycleEventListener;


public class SDKModule extends ReactContextBaseJavaModule implements LifecycleEventListener {

  ReactContext mContext;

  SDKModule(ReactApplicationContext context) {
    super(context);
    this.mContext = context;
    YappaSDK.INSTANCE.setupObserver(this.mContext, "YappaAndroidNotif",mNotificationHandler);
  }

  @NonNull
  @Override
  public String getName() {
    return "Yappasdk";
  }

  private final BroadcastReceiver mNotificationHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("YappaAndroidNotif")) {
              Log.d("SDKRN", "SdkaModule: ### Notification Received");
              Map<String, String> data = YappaSDK.INSTANCE.mapBundle(intent.getExtras());  
              WritableMap params = Arguments.createMap();
              params.putString("originalUrl", data.get("originalUrl"));
              params.putString("contentId", data.get("contentId"));
              mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("YappaAndroidNotif", params);
            }
        }
    };

  @ReactMethod
  public void initialize(String apiKey, String appId) {
      try{
          YappaSDK.INSTANCE.initialize(apiKey, Integer.parseInt(appId), this.mContext); // Lo primero que se tiene que llamar antes que nada
      }
      catch (NumberFormatException ex){
          ex.printStackTrace();
      }
  }

  @ReactMethod
  public void setContentId(String contentId) {
    YappaSDK.INSTANCE.setContentId(contentId);
  }

  @ReactMethod
  public void show() {
    YappaSDK.INSTANCE.show();
  }

  @ReactMethod
  public void close() {
    YappaSDK.INSTANCE.close();
  }

  @ReactMethod
  public void setFCMToken(String token) {
    if (!token.isEmpty()) {
      YappaSDK.INSTANCE.setFCMToken(token);
    }
  }

  @ReactMethod
  public void setContentUrl(String contentUrl) {
    YappaSDK.INSTANCE.setContentUrl(contentUrl);
  }


  @ReactMethod
  public void initializeFromNotification() {
    YappaSDK.INSTANCE.initializeFromNotification();
  }

  @ReactMethod
  public void setupAndroidObserver() {
    YappaSDK.INSTANCE.setupObserver(this.mContext, "YappaAndroidNotif",mNotificationHandler);
  }

  @ReactMethod
  public void getNotificationData(Promise promise) {
    WritableMap map = Arguments.createMap();
    Map<String, String> cachedData = YappaSDK.INSTANCE.getCachedNotificationData();

    if(cachedData != null){
      map.putString("contentUrl", cachedData.get("originalUrl"));
      map.putString("contentId", cachedData.get("contentId"));
      promise.resolve(map);
    }
    else {
      YappaSDK.INSTANCE.setCachedDataCallback((s) -> {
        map.putString("contentUrl", s.get("originalUrl"));
        map.putString("contentId", s.get("contentId"));        
        promise.resolve(map);
        return null;
      });
    }
  }




  @ReactMethod
  public void handleNotification(ReadableMap remoteMessageMap) {
    System.out.println("SdkModule:handleNotification");

    ReadableMap notification = remoteMessageMap.getMap("notification");
    ReadableMap data = remoteMessageMap.getMap("data");
    Bundle mappedData = new Bundle();

    if(data != null && notification != null){
      for (String key : data.toHashMap().keySet()) {
        if(data.getType(key) == ReadableType.String){
          mappedData.putString(key, data.getString(key));
        }
      }

      if(notification.hasKey("title") && notification.hasKey("body")){
        YappaSDK.INSTANCE.handleNotification(mappedData, (s, s2) -> {
           Log.d("SDKRN", "SdkModule:YappaSDK.handleNotification");
          return null;
        });
      }
    }
  }

  @ReactMethod
  public void handleNotificationForeground(ReadableMap remoteMessageMap) {
    ReadableMap notification = remoteMessageMap.getMap("notification");
    ReadableMap data = remoteMessageMap.getMap("data");
    HashMap<String, String> mappedData = new HashMap<>();
    if(data != null && notification != null){
      for (String key : data.toHashMap().keySet()) {
        if(data.getType(key) == ReadableType.String){
            mappedData.put(key, data.getString(key));
        }
      }
      if(notification.hasKey("title") && notification.hasKey("body")){
        YappaSDK.INSTANCE.displayNotification(mappedData, notification.getString("title"), notification.getString("body"));
      }
    }
  }


      @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {
     YappaSDK.INSTANCE.removeObserver(this.mContext,mNotificationHandler);
    }
}
