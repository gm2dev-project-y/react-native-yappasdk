import * as React from 'react';

import { StyleSheet, View, Text, TextInput } from 'react-native';
import YappaSDK, { YappaActionButton } from 'react-native-yappasdk-app';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function App() {
  const DEFAULT_URL = 'https://qa-site.yappaapp.com/';
  const DEFAULT_CONTENT_ID = "1234";
  const [currentUrl, setCurrentUrl] = React.useState(DEFAULT_URL);
  const [contentId, setContentId] = React.useState(DEFAULT_CONTENT_ID);


  React.useEffect(() => {
    YappaSDK.initialize('86d1968a7fe9e13f68825e057028abf7', '1'); // AppID
    console.log('SDK', 'Yappa initialization');

    (async () => {
      const url = DEFAULT_URL;
      const contentId = DEFAULT_CONTENT_ID;
      try {
        const value = await AsyncStorage.getItem('contentUrl');
        if (value !== null) {
          setCurrentUrl(value);
        } else {
          setCurrentUrl(url);
        }

        handleNotifications();


      } catch (e) {
        await AsyncStorage.setItem('contentUrl', url);
        setCurrentUrl(url);
        handleNotifications();
      }
    })();
  }, []);


  const handleNotifications = () => {
      YappaSDK.handleRemoteNotification(
        (contentId: string, contentUrl: string, showSdk) => {
          console.log('SDK', 'handleRemoteNotification');
          console.log(' @@@@@@ HandleNotifications Received ', contentId, contentUrl);
          // Handle app navigation
         //   YappaSDK.setContentUrl("https://variety.com/")
            YappaSDK.setContentId(contentId)
          showSdk();
        }
      );
  }

  const onUrlChange = async (text: any) => {
    setCurrentUrl(text);
    await AsyncStorage.setItem('contentUrl', text);
  };

  const onContentIdChange = async (text: any) => {
    setContentId(text);
    await AsyncStorage.setItem('contentId', text);
  };

  return (
    <View style={styles.container}>
      <Text>YappaSDK React Native</Text>
      <TextInput
        style={styles.input}
        onChangeText={onUrlChange}
        value={currentUrl}
      />
       <TextInput
        style={styles.input}
        onChangeText={onContentIdChange}
        value={contentId}
      />
      <YappaActionButton
        contentUrl={currentUrl} //https://variety.com/
        contentId={contentId} //1235070678
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  input: {
    width: "90%",
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});
